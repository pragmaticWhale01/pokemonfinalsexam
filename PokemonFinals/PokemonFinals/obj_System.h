#pragma once
#include "obj_Pokemon.h"
#include "obj_Player.h"
#include <string>
#include <vector>
#include <conio.h>
#include <time.h>
#include <stdlib.h>
using namespace std;
class obj_System
{
public:
	obj_System();
	obj_System(int y, int x);
	string input;
	string locale;
	int y;
	int x;
	int  drawMap(obj_System* bounds, obj_System* player, obj_System* safe, vector <vector<char> > elements);
	vector <obj_Pokemon> pokemons;
	vector <vector<char> >elements;
	vector<obj_Pokemon> encounters;
	void move(obj_System* player, obj_System* bounds);
	void pokemonGet(obj_Pokemon* pokemon, vector <obj_Pokemon*> pokemons);
	void showLocale(obj_System* player, obj_System* safe);
	//void encounter(obj_Player* me, obj_Pokemon* random);
	void showStatus(vector <obj_Pokemon*> pokemons);
	~obj_System();
};