#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "obj_Pokemon.h"

using namespace std;
class obj_Player
{
public:
	obj_Player();
	obj_Player(string playerName, vector <obj_Pokemon*> pokemons);
		string playerName;
		vector <obj_Pokemon> pokemons;
private:
	int pokemonCount;
	~obj_Player();
};

