#pragma once
#include <iostream>
#include <vector>
#include <string>
using namespace std;

class obj_Pokemon
{
public:
	obj_Pokemon();
	obj_Pokemon(string pokemonName, float baseHp, int hp, int level, float baseDamage, float exp, float expToNextLevel,float expGive);
	string pokemonName;
	float baseHp;
	int hp;
	int level;
	float baseDamage;
	float exp;
	float expToNextLevel;
	float expGive;
	void attack(obj_Pokemon* target);
	void scale();
	~obj_Pokemon();
};

