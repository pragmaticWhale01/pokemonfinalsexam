#include "obj_System.h"
#include "obj_Pokemon.h"
#include "obj_Player.h"
#include <iostream>
#include <string>
#include <vector>
#include <conio.h>
using namespace std;

int main()
{
	//SYSTEM OBJECTS
	obj_System* bounds = new obj_System(20, 40);
	obj_System* player = new obj_System(10, 21);
	obj_System* safe = new obj_System(8, 14);
	//POKEDEX
	obj_Pokemon* Snivy = new obj_Pokemon("Snivy", 45, 45, 1, 45, 0, 62, 31) ;
	obj_Pokemon* Tepig = new obj_Pokemon("Tepig", 65, 65, 1, 63, 0, 62, 31);
	obj_Pokemon* Oshawott = new obj_Pokemon("Oshawott", 55, 55, 1, 55, 0, 62, 31);
	obj_Pokemon* Patrat = new obj_Pokemon("Patrat", 45, 45, 1, 55, 0, 51, 25);
	obj_Pokemon* Purrloin = new obj_Pokemon("Purrloin", 41, 41, 1, 50, 0, 56, 28);
	obj_Pokemon* Pidove = new obj_Pokemon("Pidove", 50, 50, 1, 55, 0, 53, 26);
	obj_Pokemon* Sewaddle = new obj_Pokemon("Sewaddle", 45, 45, 1, 53, 0, 62, 31);
	obj_Pokemon* Sunkern = new obj_Pokemon("Sunkern", 30, 30, 1, 30, 0, 36, 18);
	obj_Pokemon* Lillipup = new obj_Pokemon("Lillipup", 45, 45, 1, 60, 0, 55, 23);
	obj_Pokemon* Mareep = new obj_Pokemon("Mareep", 55, 55, 1, 40, 0, 56, 28);
	obj_Pokemon* Psyduck = new obj_Pokemon("Psyduck", 50, 50, 1, 52, 0, 64, 32);
	obj_Pokemon* Azurill = new obj_Pokemon("Azurill", 50, 20, 1, 20, 0, 38, 19);
	obj_Pokemon* Riolu = new obj_Pokemon("Riolu", 40, 40, 1, 70, 0, 57, 28);
	obj_Pokemon* Dunsparce = new obj_Pokemon("Dunsparce", 100, 100, 1, 70, 0, 145, 72);
	obj_Pokemon* Audino = new obj_Pokemon("Audino", 103, 103, 1, 60, 0, 390, 180);
	//PLAYER
	vector <obj_Pokemon*> myPokemons;
	obj_Player* me = new obj_Player("Default", myPokemons );
	int pokemonCount;
	string input;
	int numput;
	//INTRO DIALOGUE
	cout << "Hi There!"<< endl;
	system("pause");
	system("CLS");
	cout << "Welcome ot the world of Pokemon!" << endl;
	system("pause");
	system("CLS");
	cout << "My name is Professorr Juniper. Everyone calls me the Pokemon Professor!" << endl;
	system("pause");
	system("CLS");
	cout << "That's right! This world is widely inhabited by mysterious creatures called Pok�mon! " << endl;
	system("pause");
	system("CLS");
	cout << "Could you tell me about yourself? I'd like to know your name. Please tell me." << endl;
	cin >> me->playerName;
	system("CLS");
	cout << "So your name's ''" << me->playerName << "''What a wonderful name!" << endl;
	system("pause");
	system("CLS");
	cout << "So, where are the Pok�mon ? They were delivered to " << me->playerName << "'s house so " << me->playerName << "gets first pick." << endl;
	system("pause");
	system("CLS");
	cout << "Ooh! What kind of Pok�mon could they be? \n \n Choose your starting Pokemon:\n[1]Snivy \n[2]Tepig \n[3]Oshawott" << endl;
	cin >> numput;
	switch (numput)
	{
	case 1:
		myPokemons.push_back(Snivy);
		break;
	case 2:
		myPokemons.push_back(Tepig);
		break;
	case 3:
		myPokemons.push_back(Oshawott);
		break;
	}
	system("CLS");
	cout << me->playerName << "! The moment you choose the Pok�mon that will accompany you on your journey, your story will truly begin. \n During your journey, you will meet many Pok�mon and people with different personalities and points of view! \n I really hope you find what is important to you in all of these travels� That's right! Befriend new people and Pok�mon and grow as a person! \n That is the most important goal for your journey! Let's go visit the world of Pok�mon!";

	//MAIN
	vector <vector<char> >elements;
	while (true)
	{
		system("CLS");
		safe->drawMap(bounds, player, safe, elements);
		safe->showLocale(player, safe);
		safe->showStatus(myPokemons);
		safe->move(player, bounds);
		cout << myPokemons.size();
	}
}