#include "obj_System.h"
#include "obj_Pokemon.h"
#include "obj_Player.h"
#include <Windows.h>
#include <iostream>
#include <string>
#include <vector>
#include <conio.h>
#include <time.h>
#include <stdlib.h>
using namespace std;
obj_System::obj_System()
{
 this -> y;
 this-> x;
}
obj_System::obj_System(int y, int x)
{
	this->y = y;

	this->x = x;
}
int obj_System::drawMap(obj_System * bounds, obj_System * player, obj_System * safe, vector<vector<char>> elements)
{
	//Generation
	elements.resize(bounds -> y, vector<char>(bounds->x) );
	for (int i = 0; i < elements.size(); )
	{
		for (int j = 0; j < elements[i].size();)
		{
			if (i == 0 || j == 0 || j == elements[i].size() - 1 || i == bounds->y - 1)
			{
				elements[i].push_back('X');
			}
			else if (j == player->x && i == player->y)
			{
				elements[i].push_back('O');
			}
			else if (j >= safe->x && i >= safe->y && j <= safe->x + 5 && i <= safe->y + 5)
			{
				elements[i].push_back('#');
			}
			else
			{
				elements[i].push_back(' ');
			}
			elements[i].erase(elements[i].begin());
			j++;
		}
		i++;
	}
	//Printing
	for (size_t i = 0; i < elements.size(); )
	{
		for (size_t j = 0; j < elements[i].size(); )
		{
			cout << elements[i][j];
			j++;
		}
		i++;
		cout << endl;
	}
	return 0;
}
//Move
void obj_System::move(obj_System*player, obj_System* bounds)
{
	_getch();
	if (GetAsyncKeyState(VK_UP))
	{
		if (player -> y < 2 )
		{		}
		else
		{
			player->y--;
		}
	}
	else if (GetAsyncKeyState(VK_DOWN))
	{
		if (player-> y > bounds -> y-3)
		{		}
		else
		{
			player->y++;
		}
	}
	else if (GetAsyncKeyState(VK_LEFT))
	{
		if (player->x < 2)
		{		}
		else
		{
			player-> x--;
		}
	}
	else if (GetAsyncKeyState(VK_RIGHT))
	{
		if (player->x > bounds->x -3)
		{		}
		else
		{
			player->x++;
		}
	}
	else
	{	}
}
void obj_System::pokemonGet(obj_Pokemon* pokemon, vector <obj_Pokemon*> pokemons)
{
	pokemons.push_back(pokemon);
}
void obj_System::showLocale(obj_System* player, obj_System* safe)
{
	if (player->x >= safe->x && player->y >= safe->y && player->x <= safe->x +5 && player->y <= safe->y+5)
	{
		locale = "Poke Center";
		cout << "Area: " << locale << endl << player->x << ", " << player->y << endl;
		
	}
	else
	{
		locale = "Wilderness";
		cout << "Area: " << locale << endl << player->x << ", " << player->y << endl;
	}
}
void obj_System::showStatus( vector <obj_Pokemon*> pokemons)
{
	for (int i = 0; i < pokemons.size(); )
	{
		cout << pokemons[i]->pokemonName << endl << "Level: " << pokemons[i]->level<< endl << "Hp: " << pokemons[i]->hp << endl;
		i++;
	}
}
/* void obj_System::encounter(obj_Player* me, obj_Pokemon* random)
{
	system("CLS");
	for (int i = 0; i < 4)
	{
		cout << random->pokemonName << endl << "Level: " << random->level << endl << random->hp << endl <<
			"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl <<
			me->playerName << "'s Pokemon" << endl <<
			me->pokemons;
			}
			}
			*/
obj_System::~obj_System()
{
}
