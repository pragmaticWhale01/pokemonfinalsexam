#include "obj_Pokemon.h"



obj_Pokemon::obj_Pokemon()
{
	this->pokemonName;
	this->baseHp;
	this->hp;
	this->level;
	this->baseDamage;
	this->exp;
	this->expToNextLevel;
	this->expGive;
}

obj_Pokemon::obj_Pokemon(string pokemonName, float baseHp, int hp, int level, float baseDamage, float exp, float expToNextLevel, float expGive)
{
	this->pokemonName = pokemonName;
	this->baseHp = baseHp;
	this->hp = hp;
	this->level = level;
	this->baseDamage = baseDamage;
	this->exp = exp;
	this->expToNextLevel = expToNextLevel;
	this->expGive = expGive;
}
void obj_Pokemon::attack(obj_Pokemon * target)
{
	target->hp -= this->baseDamage;
}
void obj_Pokemon::scale()
{
	this->baseHp = this->baseHp * 0.15;
	this->baseDamage = this->baseDamage * 0.1;
	this->exp = this->expToNextLevel * 0.20;
	this->expGive = this->expToNextLevel * 0.20;
}
obj_Pokemon::~obj_Pokemon()
{
}
